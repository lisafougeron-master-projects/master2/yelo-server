const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const swaggerUi = require('swagger-ui-express');
const swaggerJsdoc = require('swagger-jsdoc');

const apiRouter = require('./routes/api');
const cors = require('cors');

const app = express();

const corsOptions = {
    origin: '*',
  }
app.use(cors(corsOptions));

// Initial configuration and setup
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Various server options
// Swagger JSdoc options
const swaggerUiOptions = {
    swaggerDefinition: {
        openapi: '3.0.0',
        info: {
            title: 'Challenge Yelo API',
            version: '0.1',
            description: 'API pour l\'application Challenge Yelo',
        },
        servers: [
            {
                url: 'https://yelo-api.herokuapp.com/api/v1/',
                description: 'Production server'
            },
            {
                url: 'http://localhost:3000/api/v1',
                description: 'Local testing server'
            }
        ],
    },
    apis: ['./routes/api.js'],
};
const swaggerDocument = swaggerJsdoc(swaggerUiOptions);

// Swagger UI options
const swaggerOptions = {
    explorer: true
};

// Routes setup
app.use('/api/v1/', apiRouter);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument, swaggerOptions));

module.exports = app;
