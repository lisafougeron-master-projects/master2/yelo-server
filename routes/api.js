const express = require('express');
const router = express.Router();
const request = require('request');
const geolib = require('geolib');

let bikes = {};
let buses = {};

async function requestBikes() {
  request('https://yelo.agglo-larochelle.fr/yelo-api/-/data/bikes/live/real-time.json', function (error, response, body) {
    if (!error && response.statusCode == 200) {
      bikes = JSON.parse(body);
    } else {
      return;
    }
  });
}

async function requestBuses() {
  request('https://yelo.agglo-larochelle.fr/yelo-api/-/data/buses/geojson/all.json', function (error, response, body) {
    if (!error && response.statusCode == 200) {
      buses = JSON.parse(body).features;
    } else {
      return;
    }
  });
}

requestBikes();
requestBuses();
setInterval(() => {
  requestBikes();
  requestBuses();
}, 10000);

/**
 * @swagger
 *
 * /:
 *  get:
 *    description: Default route
 *    produces:
 *      - text/html
 *    responses:
 *      200:
 *        description: API Version 0.1
 */
router.get('/', function (_req, res, _next) {
  res.send("API Version 0.1");
});

/**
 * @swagger
 *
 * /bikes?:
 *  get:
 *    description: Merge same stations and Get bikes that match the filter and search parameters
 *    parameters:
 *      - name: filter
 *        description: the filter criteria
 *        in: query
 *        required: false
 *        schema:
 *          type: string
 *          default: {'Temps':true,'Max':5,'Type':walking,'Position':{'latitude':46.1551669444,'longitude': -1.1491938889}}
 *      - name: search
 *        description: the search criteriname
 *        in: query
 *        type: string
 *        required: false
 *        schema:
 *          type: string
 *          default: porte
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: The bikes JSON informations
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                type: object
 *                properties:
 *                  name:
 *                    type: string
 *                  freeBikes:
 *                    type: integer
 *                  free_bikes:
 *                    type: integer
 *                  emptySlots:
 *                    type: integer
 *                  empty_slots:
 *                    type: integer
 *                  latitude:
 *                    type: number
 *                  longitude:
 *                    type: number
 *                exemple:
 *                  - name: "09 Porte St Nicolas"
 *                    freeBikes: 9
 *                    free_bikes: 9
 *                    emptySlots: 7
 *                    empty_slots: 7
 *                    latitude: 46.1557038889
 *                    longitude: -1.1495629166499999
 *
 */
router.get('/bikes?', function (_req, res, _next) {
  let filterParam = _req.query.filter;
  let searchParam = _req.query.search;
  let filteredBikes = conbineBikesStation(bikes);
  if (searchParam) {
    filteredBikes = filteredBikes.filter(item => item.name.toUpperCase().includes(searchParam.toUpperCase()));
  }
  if (filterParam) {
    filteredBikes = fileterStations(filteredBikes, JSON.parse(filterParam));
  }

  res.status(200).json(filteredBikes);
})

/**
 * @swagger
 *
 * /buses?:
 *  get:
 *    description: Get buses that match the filter and search parameters
 *    parameters:
 *      - name: filter
 *        description: the filter criteria
 *        in: query
 *        required: false
 *        schema:
 *          type: string
 *          default: {'Temps':true,'Max':5,'Type':walking,'Position':{'latitude':46.1551669444,'longitude': -1.1491938889}}
 *      - name: search
 *        description: the search criteriname
 *        in: query
 *        type: string
 *        required: false
 *        schema:
 *          type: string
 *          default: porte
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: The buses JSON informations
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                type: object
 *                properties:
 *                  name:
 *                    type: string
 *                  latitude:
 *                    type: number
 *                  longitude:
 *                    type: number
 *                exemple:
 *                  - name: "Le Rayon d'Or"
 *                    latitude: -1.14726810065415
 *                    longitude: 46.19138463303994
 *
 */
router.get('/buses?', function (_req, res, _next) {
  let filterParam = _req.query.filter;
  let searchParam = _req.query.search;
  for (const bus of buses) {
    bus.longitude = bus.geometry.coordinates[0];
    bus.latitude = bus.geometry.coordinates[1];
    bus.name = bus.properties.name;
    bus.url = bus.properties.url;
  }
  let filteredBuses = combineBuses(buses);
  if (searchParam) {
    filteredBuses = filteredBuses.filter(item => item.name.toUpperCase().includes(searchParam.toUpperCase()));
  }
  if (filterParam) {
    filteredBuses = fileterStations(filteredBuses, JSON.parse(filterParam));
  }

  res.status(200).json(filteredBuses);
})

function combineBuses(stations) {
  let filteredStations = [];
  stations.forEach(element => {
    element.name = element.name.replace(/ (\d+)(?!.*\d)/, '')
    filteredStations.push(element);
  });
  const groupBy = filteredStations.reduce(function (r, a) {
    if (!r[a.name]) {
      r[a.name] = { ...a, count: 1, urls: [a.url] };
      return r;
    }
    r[a.name].latitude += a.latitude;
    r[a.name].longitude += a.longitude;
    r[a.name].count += 1;
    if (r[a.name].urls == undefined) {
      r[a.name].urls = [];
    }
    r[a.name].urls.push(a.url);

    return r;
  }, Object.create(null));

  const combineStations = Object.keys(groupBy).map(function (k) {
    const item = groupBy[k];
    return {
      name: item.name,
      latitude: item.latitude / item.count,
      longitude: item.longitude / item.count,
      urls: item.urls
    }
  })
  return combineStations;
}

function calulDistance(begin, end) {
  //la rochelle [ 46.1551669444, -1.1491938889]
  return L.map.distance([46.1551669444, -1.1491938889], [end.latitude, end.longitude]);
}

function fileterStations(stations, filterParam) {
  if (!filterParam.Temps) {
    if (filterParam.Max !== '' && filterParam.Max !== undefined) {
      stations = stations.filter(item =>
        geolib.getDistance(filterParam.Position, { latitude: item.latitude, longitude: item.longitude })
        <= (filterParam.Max * 1000));
    }
  } else {
    if (filterParam.Max !== '' && filterParam.Max !== undefined) {
      const vitesseMoyenne = filterParam.Type == 'walking' ? 4 : 14;
      stations = stations.filter(item =>
        (geolib.getDistance(filterParam.Position, { latitude: item.latitude, longitude: item.longitude }) / (vitesseMoyenne * 1000 / 60))
        <= filterParam.Max);
    }
  }
  return stations;
}

function conbineBikesStation(stations) {
  let filteredStations = [];
  stations.network.stations.forEach(element => {
    element.name = element.name.replace(/ (\d+)(?!.*\d)/, '')
    filteredStations.push(element);
  });
  const groupBy = filteredStations.reduce(function (r, a) {
    if (!r[a.name]) {
      r[a.name] = { ...a, count: 1 };
      return r;
    }
    r[a.name].freeBikes += a.freeBikes;
    r[a.name].free_bikes += a.free_bikes;
    r[a.name].emptySlots += a.emptySlots;
    r[a.name].empty_slots += a.empty_slots;
    r[a.name].latitude += a.latitude;
    r[a.name].longitude += a.longitude;
    r[a.name].count += 1;
    return r;
  }, Object.create(null));

  const combineStations = Object.keys(groupBy).map(function (k) {
    const item = groupBy[k];
    return {
      name: item.name,
      freeBikes: item.freeBikes,
      free_bikes: item.free_bikes,
      emptySlots: item.emptySlots,
      empty_slots: item.empty_slots,
      latitude: item.latitude / item.count,
      longitude: item.longitude / item.count,
    }
  })
  return combineStations;

}



/**
 * @swagger
 *
 * /urls:
 *  post:
 *    description: Merge same stations and Get bikes that match the filter and search parameters
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              urls:
 *                type: array
 *                items:
 *                  type: string
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: The buses JSON informations
 */
router.post('/urls', function (_req, res, _next) {
  let result = { goOn: [], comeback: [] };
  requestUrls(_req.body.urls).then( function(data) {
    data.forEach(station => {
      if(Array.isArray(station.nextBuses) && station.nextBuses.length!=0) {
        station.nextBuses.forEach(next => {
          if (next.direction.toUpperCase()=="Aller".toUpperCase()) {
            result.goOn.push(next);
          } else {
            result.comeback.push(next);
          }
        });
      }
    });
    result.goOn = sortNextBusesByDelay(result.goOn);
    result.comeback = sortNextBusesByDelay(result.comeback);
  res.status(200).json(result);
  });
});
 function sortNextBusesByDelay(next) {
  if(Array.isArray(next) && next.length!=0){
    return next.sort(function (x, y) {
      return ((x.delay === y.delay) ? 0 : ((x.delay > y.delay) ? 1 : -1));
    });
  }
  return next;
 }

function requestUrls(urls) {
  const promises = []
  urls.forEach(
    url=> promises.push(asyncRequestUrl(url)
  ));
  return Promise.all(promises);
 }
 function asyncRequestUrl(url) {
   return new Promise(function(resolve, reject) {
    request(url, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        resolve(JSON.parse(body));
      }
      reject(error, response, body);
    });
   });
 }
module.exports = router;
